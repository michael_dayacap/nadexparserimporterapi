package com.granberg.nadexparserimporter.csv.parser;

import au.com.bytecode.opencsv.CSVReader;
import com.granberg.nadexparserimporter.entity.Forex;
import com.granberg.nadexparserimporter.loader.MYSQLDBAccess;
import com.granberg.nadexparserimporter.CommonUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author mdayacap
 */
public class ForexParserImporter {

    private static final Pattern symbolPattern = Pattern.compile("(\\w{3})(\\w{3})");
    private static final String SYMBOL_FORMAT = "$1/$2";
    private static final char DEFAULT_DELIMITER = ',';
    private SimpleDateFormat inputDateFormat;
    private CSVReader reader;
    private MYSQLDBAccess loader;
    private char delimiter = DEFAULT_DELIMITER;
    private String dumpDirPath;

    public ForexParserImporter(String dumpDirPath, MYSQLDBAccess loader, String timestampFormat) {
        this.dumpDirPath = dumpDirPath;
        this.loader = loader;
        inputDateFormat = new SimpleDateFormat(timestampFormat);
    }

    public ForexParserImporter(String dumpDirPath, char delimiter, MYSQLDBAccess loader, String timestampFormat) {
        this(dumpDirPath, loader, timestampFormat);
        this.delimiter = delimiter;
    }

    public void importToDB() {
        File[] files = CommonUtils.getFilesToParseFromDir(dumpDirPath);
        List<Forex> forexList = new ArrayList<>();
        for (File file : files) {

            try {
                reader = new CSVReader(new FileReader(file), delimiter);
                boolean firstLine = true;
                for (String[] line : reader.readAll()) {
                    if (firstLine) {
                        firstLine = false;
                        continue;
                    }
                    Forex forex = new Forex();
                    forex.setSymbol(reformatSymbol(line[0]));
                    forex.setDate(CommonUtils.addTimeToDate(line[1], inputDateFormat));
                    forex.setOpen(line[2]);
                    forex.setHigh(line[3]);
                    forex.setLow(line[4]);
                    forex.setClose(line[5]);
                    if (line.length >= 7) {
                        forex.setVolume(line[6]);
                    } else {
                        forex.setVolume("0");
                    }

                    forexList.add(forex);
                }

            } catch (FileNotFoundException ex) {
                Logger.getLogger(ForexParserImporter.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ForexParserImporter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        loader.executeForexBatch(forexList, Forex.INSERT_SQL);

    }

    private String reformatSymbol(String symbol) {
        Matcher m = symbolPattern.matcher(symbol);
        if (m.matches()) {
            return m.replaceAll(SYMBOL_FORMAT);
        }
        return symbol;
    }
}
