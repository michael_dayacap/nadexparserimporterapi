package com.granberg.nadexparserimporter.importer;

import com.granberg.nadexparserimporter.controller.jpa.ForexJpaController;
import com.granberg.nadexparserimporter.controller.jpa.HourlyPipsJpaController;
import com.granberg.nadexparserimporter.entity.jpa.Forex;
import com.granberg.nadexparserimporter.entity.jpa.HourlyPips;
import com.granberg.nadexparserimporter.entity.jpa.HourlyPipsPK;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mdayacap
 */
public class HourlyPipsImporter {

    private final static String HOUR_SUFFIX = "00";
    private ForexJpaController forexController;
    private HourlyPipsJpaController hourlyPipsController;

    public HourlyPipsImporter(ForexJpaController forexController, HourlyPipsJpaController hourlyPipsController) {
        this.forexController = forexController;
        this.hourlyPipsController = hourlyPipsController;
    }

    public void importHourlyPipsWeekly(String instrumentName) {
        // Get the startDate and endDate of last week
        // Loop per day
        //  Get HourlyPips - importHourlyPips(String instrumentName, Date startDate, Date endDate);
        //  Store HourlyPips in a Map<Hour, List<Pips>>
        // ave = computeAveragePips(map.get(Hour))
        // Store ave -> Map<Hour, AvePip>
        // Update HourlyPips Weekly -> Map<Hour, AvePip>
    }

    public void importHourlyPips30DayMovingAverage(String instrumentName) {
        // TODO
        // Date startDate = currentDate
        // Date endDate = currentDate - 30 days
        // importHourlyPips(String instrumentName, Date startDate, Date endDate);
    }

    public void importHourlyPips(String instrumentName, Date endDate, int hours, int movingAverageInDays) {

        if ((24 % hours) != 0) {
            Logger.getLogger(HourlyPipsImporter.class.getName()).log(Level.SEVERE, "HOURS (interval) should be factors of 24");
        }

        TreeSet<Date> days = new TreeSet<Date>();
        days.add(endDate);
        Date today = endDate;
        Calendar yesterday = Calendar.getInstance();
        int ctr = 1;
        while (ctr < movingAverageInDays) {
            yesterday.setTime(today);
            yesterday.add(Calendar.DAY_OF_YEAR, -1);
            today = yesterday.getTime();
            days.add(today);
            ctr++;
        }

        List<HourlyPips> hourlyPipsList = new ArrayList<HourlyPips>();
        List<Integer> pips = null;
        int interval = hours - 1;
        for (int i = 0; i < 24; i = i + hours) {
            pips = new ArrayList<Integer>();
            Calendar startTime = Calendar.getInstance();
            Calendar endTime = Calendar.getInstance();
            for (Date day : days) {
                startTime.setTime(day);
                startTime.add(Calendar.HOUR_OF_DAY, i);

                endTime.setTime(startTime.getTime());
                endTime.add(Calendar.HOUR_OF_DAY, interval);
                endTime.add(Calendar.MINUTE, 59);
                endTime.add(Calendar.SECOND, 59);
                // Get time intervals for an Hour e.g. 00:00:00 - 00:59:59

                List<Forex> forexListGroupedByHour = forexController.findBySymbolAndBetweenDates(instrumentName, startTime, endTime);
                Integer pip = 0;
                if (forexListGroupedByHour != null && !forexListGroupedByHour.isEmpty()) {
                    pip = computePipFromForexGroup(instrumentName, forexListGroupedByHour);
                }
                pips.add(pip);

            }
            Integer ave = getAveragePip(pips);
            HourlyPips hourlyPips = createHourlyPips(instrumentName, hours, i, ave, movingAverageInDays, endDate);
            hourlyPipsList.add(hourlyPips);

        }


        try {
            for (HourlyPips hourlyPips : hourlyPipsList) {
                hourlyPipsController.createOrEdit(hourlyPips);
            }
        } catch (Exception ex) {
            Logger.getLogger(HourlyPipsImporter.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private Integer computePipFromForexGroup(String instrumentName, List<Forex> forexListGroupedByHour) {

        BigDecimal minOpen = getMinimumOpenFromForexList(forexListGroupedByHour);
        BigDecimal maxClose = getMaximumCloseFromForexList(forexListGroupedByHour);
        BigDecimal pip = null;
        if ("EUR/JPY".equals(instrumentName)) {
            pip = minOpen.subtract(maxClose).abs().multiply(BigDecimal.valueOf(100));
        } else {
            pip = minOpen.subtract(maxClose).abs().multiply(BigDecimal.valueOf(10000));
        }
        return pip.intValue();
    }

    private BigDecimal getMinimumOpenFromForexList(List<Forex> forexListGroupedByHour) {

        TreeSet<BigDecimal> openSet = new TreeSet<BigDecimal>();
        for (Forex forex : forexListGroupedByHour) {
            openSet.add(forex.getOpen());
        }

        return openSet.first();
    }

    private BigDecimal getMaximumCloseFromForexList(List<Forex> forexListGroupedByHour) {
        TreeSet<BigDecimal> closeSet = new TreeSet<BigDecimal>();
        for (Forex forex : forexListGroupedByHour) {
            closeSet.add(forex.getClose());
        }

        return closeSet.last();
    }

    private Integer getAveragePip(List<Integer> pips) {

        if (pips.size() == 0) {
            return 0;
        }

        Integer total = new Integer(0);
        for (Integer pip : pips) {
            total = pip + total;
        }

        Integer ave = total / pips.size();

        return ave;
    }

    private HourlyPips createHourlyPips(String symbol, int interval, int startHr, Integer pip, int movingAverageInDays, Date endDate) {

        int endHr = startHr + interval;
        String startHour = formatHour(startHr);
        String endHour = formatHour(endHr);

        HourlyPipsPK hourlyPipsPK = new HourlyPipsPK(symbol, startHour, endHour, movingAverageInDays);
        HourlyPips hourlyPips = new HourlyPips(pip, interval, endDate);
        hourlyPips.setHourlyPipsPK(hourlyPipsPK);
        return hourlyPips;
    }

    private String formatHour(int hour) {
        return String.format("%02d", hour) + HOUR_SUFFIX;
    }
}
