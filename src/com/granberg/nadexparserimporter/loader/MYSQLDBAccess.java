/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexparserimporter.loader;

import com.granberg.nadexparserimporter.entity.Forex;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mdayacap
 */
public class MYSQLDBAccess {

    private Connection connection;
    private PreparedStatement statement;
    private String url;

    public void setUrl(String dbUrl) {
        this.url = dbUrl;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }
    private String driver;

    private MYSQLDBAccess() {
    }

    public static MYSQLDBAccess getInstance() {
        return MySQLDBLoaderHolder.INSTANCE;
    }

    public void connect() {
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(MYSQLDBAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void executeForexBatch(List<Forex> forexList, String insertSQL) {
        try {
            statement = connection.prepareStatement(insertSQL);

            int ctr = 0;
            for (Forex forex : forexList) {
                ctr++;
                statement.setString(1, forex.getSymbol());
                statement.setTimestamp(2, new Timestamp(forex.getDate().getTime()));
                statement.setDouble(3, Double.parseDouble(forex.getOpen()));
                statement.setDouble(4, Double.parseDouble(forex.getHigh()));
                statement.setDouble(5, Double.parseDouble(forex.getLow()));
                statement.setDouble(6, Double.parseDouble(forex.getClose()));
                statement.setDouble(7, Integer.parseInt(forex.getVolume()));
                statement.addBatch();

                if (ctr == 1000) {
                    statement.executeBatch();
                }
            }
            statement.executeBatch();
        } catch (SQLException ex) {
            Logger.getLogger(MYSQLDBAccess.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close();
        }
    }

    private void close() {
        try {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(MYSQLDBAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static class MySQLDBLoaderHolder {

        private static final MYSQLDBAccess INSTANCE = new MYSQLDBAccess();
    }
}
