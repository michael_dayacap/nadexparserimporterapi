/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexparserimporter.controller.jpa;

import com.granberg.nadexparserimporter.controller.jpa.exceptions.NonexistentEntityException;
import com.granberg.nadexparserimporter.entity.jpa.Forex;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author mdayacap
 */
public interface ForexJpaController {
    void create(Forex forex);
    void edit(Forex forex) throws NonexistentEntityException, Exception;
    void destroy(Integer id) throws NonexistentEntityException;
    List<Forex> findForexEntities();
    List<Forex> findForexEntities(int maxResults, int firstResult);
    Forex findForex(Integer id);
    int getForexCount();    

    List<Forex> findBySymbolAndBetweenDates(String instrumentName, Date startDate, Date endDate);

    List<Forex> findBySymbolAndBetweenDates(String instrumentName, Calendar startTime, Calendar endTime);
}
