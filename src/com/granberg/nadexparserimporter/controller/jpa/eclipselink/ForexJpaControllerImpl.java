/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexparserimporter.controller.jpa.eclipselink;

import com.granberg.nadexparserimporter.controller.jpa.ForexJpaController;
import com.granberg.nadexparserimporter.controller.jpa.exceptions.NonexistentEntityException;
import com.granberg.nadexparserimporter.entity.jpa.Forex;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TemporalType;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author mdayacap
 */
public class ForexJpaControllerImpl implements ForexJpaController, Serializable {

    private EntityManagerFactory emf;
    private EntityManager em;

    public ForexJpaControllerImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Forex forex) {
        em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(forex);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Forex forex) throws NonexistentEntityException, Exception {
        em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            forex = em.merge(forex);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = forex.getForexId();
                if (findForex(id) == null) {
                    throw new NonexistentEntityException("The forex with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Forex forex;
            try {
                forex = em.getReference(Forex.class, id);
                forex.getForexId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The forex with id " + id + " no longer exists.", enfe);
            }
            em.remove(forex);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Forex> findForexEntities() {
        return findForexEntities(true, -1, -1);
    }

    public List<Forex> findForexEntities(int maxResults, int firstResult) {
        return findForexEntities(false, maxResults, firstResult);
    }

    private List<Forex> findForexEntities(boolean all, int maxResults, int firstResult) {
        em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Forex.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Forex findForex(Integer id) {
        em = getEntityManager();
        try {
            return em.find(Forex.class, id);
        } finally {
            em.close();
        }
    }

    public int getForexCount() {
        em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Forex> rt = cq.from(Forex.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public List<Forex> findBySymbolAndBetweenDates(String instrumentName, Date startDate, Date endDate) {
        em = getEntityManager();
        List<Forex> forexList = em.createNamedQuery("Forex.findBySymbolAndBetweenDates", Forex.class).setParameter("symbol", instrumentName).setParameter("startDate", startDate, TemporalType.TIMESTAMP).setParameter("endDate", endDate, TemporalType.TIMESTAMP).getResultList();


        return forexList;
    }

    @Override
    public List<Forex> findBySymbolAndBetweenDates(String instrumentName, Calendar startTime, Calendar endTime) {
        em = getEntityManager();
        return em.createNamedQuery("Forex.findBySymbolAndBetweenDates", Forex.class).setParameter("symbol", instrumentName).setParameter("startDate", startTime, TemporalType.TIMESTAMP).setParameter("endDate", endTime, TemporalType.TIMESTAMP).getResultList();
    }
}
