/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexparserimporter.controller.jpa.eclipselink;

import com.granberg.nadexparserimporter.controller.jpa.HourlyPipsJpaController;
import com.granberg.nadexparserimporter.controller.jpa.exceptions.NonexistentEntityException;
import com.granberg.nadexparserimporter.controller.jpa.exceptions.PreexistingEntityException;
import com.granberg.nadexparserimporter.entity.jpa.HourlyPips;
import com.granberg.nadexparserimporter.entity.jpa.HourlyPipsPK;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author mdayacap
 */
public class HourlyPipsJpaControllerImpl implements HourlyPipsJpaController, Serializable {

    private EntityManagerFactory emf = null;

    public HourlyPipsJpaControllerImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public void create(HourlyPips hourlyPips) throws PreexistingEntityException, Exception {
        if (hourlyPips.getHourlyPipsPK() == null) {
            hourlyPips.setHourlyPipsPK(new HourlyPipsPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(hourlyPips);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findHourlyPips(hourlyPips.getHourlyPipsPK()) != null) {
                throw new PreexistingEntityException("HourlyPips " + hourlyPips + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void edit(HourlyPips hourlyPips) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            hourlyPips = em.merge(hourlyPips);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                HourlyPipsPK id = hourlyPips.getHourlyPipsPK();
                if (findHourlyPips(id) == null) {
                    throw new NonexistentEntityException("The hourlyPips with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void destroy(HourlyPipsPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            HourlyPips hourlyPips;
            try {
                hourlyPips = em.getReference(HourlyPips.class, id);
                hourlyPips.getHourlyPipsPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The hourlyPips with id " + id + " no longer exists.", enfe);
            }
            em.remove(hourlyPips);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<HourlyPips> findHourlyPipsEntities() {
        return findHourlyPipsEntities(true, -1, -1);
    }

    @Override
    public List<HourlyPips> findHourlyPipsEntities(int maxResults, int firstResult) {
        return findHourlyPipsEntities(false, maxResults, firstResult);
    }

    private List<HourlyPips> findHourlyPipsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(HourlyPips.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public HourlyPips findHourlyPips(HourlyPipsPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(HourlyPips.class, id);
        } finally {
            em.close();
        }
    }

    @Override
    public int getHourlyPipsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<HourlyPips> rt = cq.from(HourlyPips.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public void createOrEdit(HourlyPips hourlyPips) {
        try {
            HourlyPips existingHourlyPips = findHourlyPips(hourlyPips.getHourlyPipsPK());
            if (existingHourlyPips == null) {
                create(hourlyPips);
            } else {
                edit(hourlyPips);
            }
        } catch (PreexistingEntityException ex) {
            Logger.getLogger(HourlyPipsJpaControllerImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(HourlyPipsJpaControllerImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
