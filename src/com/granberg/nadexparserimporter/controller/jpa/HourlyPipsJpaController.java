/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexparserimporter.controller.jpa;

import com.granberg.nadexparserimporter.controller.jpa.exceptions.NonexistentEntityException;
import com.granberg.nadexparserimporter.controller.jpa.exceptions.PreexistingEntityException;
import com.granberg.nadexparserimporter.entity.jpa.HourlyPips;
import com.granberg.nadexparserimporter.entity.jpa.HourlyPipsPK;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author mdayacap
 */
public interface HourlyPipsJpaController {
    public EntityManager getEntityManager();
    public void create(HourlyPips hourlyPips) throws PreexistingEntityException, Exception;
    public void edit(HourlyPips hourlyPips) throws NonexistentEntityException, Exception;
    public void destroy(HourlyPipsPK id) throws NonexistentEntityException;
    public List<HourlyPips> findHourlyPipsEntities();
    public List<HourlyPips> findHourlyPipsEntities(int maxResults, int firstResult);
    public HourlyPips findHourlyPips(HourlyPipsPK id);
    public int getHourlyPipsCount();
    public void createOrEdit(HourlyPips hourlyPips); 
}
