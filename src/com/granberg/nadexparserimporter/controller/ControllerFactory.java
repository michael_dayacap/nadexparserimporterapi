/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexparserimporter.controller;

import com.granberg.nadexparserimporter.controller.jpa.ForexJpaController;
import com.granberg.nadexparserimporter.controller.jpa.ForexJpaController;
import com.granberg.nadexparserimporter.controller.jpa.HourlyPipsJpaController;
import com.granberg.nadexparserimporter.controller.jpa.HourlyPipsJpaController;

/**
 *
 * @author mdayacap
 */
public abstract class ControllerFactory {

    public static final int SIMULATOR = 1;
    public static final int MYSQL = 2;

    public abstract ForexJpaController getForexJpaController();
    public abstract HourlyPipsJpaController getHourlyPipsJpaController();

    public static ControllerFactory getControllerFactory(int whichFactory) {
        switch (whichFactory) {
            case SIMULATOR:
                return SimulatorControllerFactory.getInstance();
            case MYSQL:
                 return MySQLControllerFactory.getInstance();
            default:
                return null;
        }
    }
}
