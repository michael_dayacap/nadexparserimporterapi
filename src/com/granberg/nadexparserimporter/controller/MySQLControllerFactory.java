/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexparserimporter.controller;

import com.granberg.nadexparserimporter.controller.jpa.ForexJpaController;
import com.granberg.nadexparserimporter.controller.jpa.HourlyPipsJpaController;
import com.granberg.nadexparserimporter.controller.jpa.eclipselink.ForexJpaControllerImpl;
import com.granberg.nadexparserimporter.controller.jpa.eclipselink.HourlyPipsJpaControllerImpl;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author mdayacap
 */
public class MySQLControllerFactory extends ControllerFactory {

    private EntityManagerFactory emf;

    private MySQLControllerFactory() {
        emf = Persistence.createEntityManagerFactory("nadexParserImporterAPIPUMySQL");
    }

    public static MySQLControllerFactory getInstance() {
        return MySQLControllerFactoryHolder.INSTANCE;
    }

    @Override
    public ForexJpaController getForexJpaController() {
        return new ForexJpaControllerImpl(emf);
    }

    @Override
    public HourlyPipsJpaController getHourlyPipsJpaController() {
        return new HourlyPipsJpaControllerImpl(emf);
    }

    private static class MySQLControllerFactoryHolder {

        private static final MySQLControllerFactory INSTANCE = new MySQLControllerFactory();
    }
}
