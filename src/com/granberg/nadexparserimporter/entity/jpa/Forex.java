/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexparserimporter.entity.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mdayacap
 */
@Entity
@Table(name = "Forex")
@NamedQueries({
    @NamedQuery(name = "Forex.findAll", query = "SELECT f FROM Forex f"),
    @NamedQuery(name = "Forex.findByForexId", query = "SELECT f FROM Forex f WHERE f.forexId = :forexId"),
    @NamedQuery(name = "Forex.findBySymbol", query = "SELECT f FROM Forex f WHERE f.symbol = :symbol"),
    @NamedQuery(name = "Forex.findByDate", query = "SELECT f FROM Forex f WHERE f.date = :date"),
    @NamedQuery(name = "Forex.findByOpen", query = "SELECT f FROM Forex f WHERE f.open = :open"),
    @NamedQuery(name = "Forex.findByHigh", query = "SELECT f FROM Forex f WHERE f.high = :high"),
    @NamedQuery(name = "Forex.findByLow", query = "SELECT f FROM Forex f WHERE f.low = :low"),
    @NamedQuery(name = "Forex.findByClose", query = "SELECT f FROM Forex f WHERE f.close = :close"),
    @NamedQuery(name = "Forex.findByVolume", query = "SELECT f FROM Forex f WHERE f.volume = :volume"),
    @NamedQuery(name = "Forex.findBySymbolAndBetweenDates", query = "SELECT f FROM Forex f WHERE f.symbol = :symbol AND f.date BETWEEN :startDate and :endDate")})
public class Forex implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "forex_id")
    private Integer forexId;
    @Basic(optional = false)
    @Column(name = "symbol")
    private String symbol;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "open")
    private BigDecimal open;
    @Column(name = "high")
    private BigDecimal high;
    @Column(name = "low")
    private BigDecimal low;
    @Column(name = "close")
    private BigDecimal close;
    @Column(name = "volume")
    private Integer volume;

    public Forex() {
    }

    public Forex(Integer forexId) {
        this.forexId = forexId;
    }

    public Forex(Integer forexId, String symbol) {
        this.forexId = forexId;
        this.symbol = symbol;
    }

    public Integer getForexId() {
        return forexId;
    }

    public void setForexId(Integer forexId) {
        this.forexId = forexId;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getOpen() {
        return open;
    }

    public void setOpen(BigDecimal open) {
        this.open = open;
    }

    public BigDecimal getHigh() {
        return high;
    }

    public void setHigh(BigDecimal high) {
        this.high = high;
    }

    public BigDecimal getLow() {
        return low;
    }

    public void setLow(BigDecimal low) {
        this.low = low;
    }

    public BigDecimal getClose() {
        return close;
    }

    public void setClose(BigDecimal close) {
        this.close = close;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (forexId != null ? forexId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Forex)) {
            return false;
        }
        Forex other = (Forex) object;
        if ((this.forexId == null && other.forexId != null) || (this.forexId != null && !this.forexId.equals(other.forexId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.nadexparserimporter.jpa.entity.Forex[ forexId=" + forexId + ", symbol=" + getSymbol() + ", open=" + getOpen() + ", high=" + getHigh() + ", low=" + getLow() + ", close=" + getClose() + " ]";
    }
}
