/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexparserimporter.entity.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mdayacap
 */
@Embeddable
public class HourlyPipsPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "instrument_name")
    private String instrumentName;
    @Basic(optional = false)
    @Column(name = "start_hour")
    private String startHour;
    @Basic(optional = false)
    @Column(name = "end_hour")
    private String endHour;
    @Basic(optional = false)
    @Column(name = "moving_average_in_days")
    private int movingAverageInDays;

    public HourlyPipsPK() {
    }

    public HourlyPipsPK(String instrumentName, String startHour, String endHour, int movingAverageInDays) {
        this.instrumentName = instrumentName;
        this.startHour = startHour;
        this.endHour = endHour;
        this.movingAverageInDays = movingAverageInDays;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getEndHour() {
        return endHour;
    }

    public void setEndHour(String endHour) {
        this.endHour = endHour;
    }

    public int getMovingAverageInDays() {
        return movingAverageInDays;
    }

    public void setMovingAverageInDays(int movingAverageInDays) {
        this.movingAverageInDays = movingAverageInDays;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (instrumentName != null ? instrumentName.hashCode() : 0);
        hash += (startHour != null ? startHour.hashCode() : 0);
        hash += (endHour != null ? endHour.hashCode() : 0);
        hash += (int) movingAverageInDays;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HourlyPipsPK)) {
            return false;
        }
        HourlyPipsPK other = (HourlyPipsPK) object;
        if ((this.instrumentName == null && other.instrumentName != null) || (this.instrumentName != null && !this.instrumentName.equals(other.instrumentName))) {
            return false;
        }
        if ((this.startHour == null && other.startHour != null) || (this.startHour != null && !this.startHour.equals(other.startHour))) {
            return false;
        }
        if ((this.endHour == null && other.endHour != null) || (this.endHour != null && !this.endHour.equals(other.endHour))) {
            return false;
        }
        if (this.movingAverageInDays != other.movingAverageInDays) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.nadexparserimporter.entity.jpa.HourlyPipsPK[ instrumentName=" + instrumentName + ", startHour=" + startHour + ", endHour=" + endHour + ", movingAverageInDays=" + movingAverageInDays + " ]";
    }
    
}
