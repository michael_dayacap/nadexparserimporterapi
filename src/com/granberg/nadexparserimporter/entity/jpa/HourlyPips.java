/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexparserimporter.entity.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mdayacap
 */
@Entity
@Table(name = "hourly_pips")
@NamedQueries({
    @NamedQuery(name = "HourlyPips.findAll", query = "SELECT h FROM HourlyPips h"),
    @NamedQuery(name = "HourlyPips.findByInstrumentName", query = "SELECT h FROM HourlyPips h WHERE h.hourlyPipsPK.instrumentName = :instrumentName"),
    @NamedQuery(name = "HourlyPips.findByStartHour", query = "SELECT h FROM HourlyPips h WHERE h.hourlyPipsPK.startHour = :startHour"),
    @NamedQuery(name = "HourlyPips.findByEndHour", query = "SELECT h FROM HourlyPips h WHERE h.hourlyPipsPK.endHour = :endHour"),
    @NamedQuery(name = "HourlyPips.findByPip", query = "SELECT h FROM HourlyPips h WHERE h.pip = :pip"),
    @NamedQuery(name = "HourlyPips.findByHourlyInterval", query = "SELECT h FROM HourlyPips h WHERE h.hourlyInterval = :hourlyInterval"),
    @NamedQuery(name = "HourlyPips.findByMovingAverageInDays", query = "SELECT h FROM HourlyPips h WHERE h.hourlyPipsPK.movingAverageInDays = :movingAverageInDays"),
    @NamedQuery(name = "HourlyPips.findByLastDate", query = "SELECT h FROM HourlyPips h WHERE h.lastDate = :lastDate")})
public class HourlyPips implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected HourlyPipsPK hourlyPipsPK;
    @Column(name = "pip")
    private Integer pip;
    @Basic(optional = false)
    @Column(name = "hourly_interval")
    private int hourlyInterval;
    @Column(name = "last_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastDate;

    public HourlyPips() {
    }

    public HourlyPips(HourlyPipsPK hourlyPipsPK) {
        this.hourlyPipsPK = hourlyPipsPK;
    }

    public HourlyPips(HourlyPipsPK hourlyPipsPK, int hourlyInterval) {
        this.hourlyPipsPK = hourlyPipsPK;
        this.hourlyInterval = hourlyInterval;
    }

    public HourlyPips(Integer pip, Integer hourlyInterval, Date lastDate){
        this.pip = pip;
        this.hourlyInterval = hourlyInterval;
        this.lastDate = lastDate;
    }
    
    public HourlyPips(String instrumentName, String startHour, String endHour, int movingAverageInDays) {
        this.hourlyPipsPK = new HourlyPipsPK(instrumentName, startHour, endHour, movingAverageInDays);
    }

    public HourlyPipsPK getHourlyPipsPK() {
        return hourlyPipsPK;
    }

    public void setHourlyPipsPK(HourlyPipsPK hourlyPipsPK) {
        this.hourlyPipsPK = hourlyPipsPK;
    }

    public Integer getPip() {
        return pip;
    }

    public void setPip(Integer pip) {
        this.pip = pip;
    }

    public int getHourlyInterval() {
        return hourlyInterval;
    }

    public void setHourlyInterval(int hourlyInterval) {
        this.hourlyInterval = hourlyInterval;
    }

    public Date getLastDate() {
        return lastDate;
    }

    public void setLastDate(Date lastDate) {
        this.lastDate = lastDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (hourlyPipsPK != null ? hourlyPipsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HourlyPips)) {
            return false;
        }
        HourlyPips other = (HourlyPips) object;
        if ((this.hourlyPipsPK == null && other.hourlyPipsPK != null) || (this.hourlyPipsPK != null && !this.hourlyPipsPK.equals(other.hourlyPipsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.nadexparserimporter.entity.jpa.HourlyPips[ hourlyPipsPK=" + hourlyPipsPK + " ]";
    }
    
}
