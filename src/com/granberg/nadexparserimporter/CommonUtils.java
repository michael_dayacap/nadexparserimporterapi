/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexparserimporter;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mdayacap
 */
public class CommonUtils {

    public static File[] getFilesToParseFromDir(String dumpDirPath) {
        File dumpDir = new File(dumpDirPath);
        if (!dumpDir.exists() || !dumpDir.isDirectory()) {
            System.out.println("Not a directory: " + dumpDirPath);
            return null;
        }
        return dumpDir.listFiles();
    }

    public static Date addTimeToDate(String date, SimpleDateFormat inputDateFormat) {
        Date inputDate = null;
        try {
            inputDate = inputDateFormat.parse(date);
        } catch (ParseException ex) {
            Logger.getLogger(CommonUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return inputDate;
    }
}
