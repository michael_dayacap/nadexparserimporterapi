/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexparserimporter.controller.jpa;

import static org.junit.Assert.*;

import com.granberg.nadexparserimporter.controller.ControllerFactory;
import com.granberg.nadexparserimporter.entity.jpa.Forex;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TreeSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author mdayacap
 */
public class ForexJpaControllerTest {

    public ForexJpaControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void findBySymbolAndBetweenDates() throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = df.parse("2013-07-11 00:00:00");
        Date endDate = df.parse("2013-07-12 23:59:59");

        ForexJpaController forexJpaController = ControllerFactory.getControllerFactory(ControllerFactory.MYSQL).getForexJpaController();
        List<Forex> forexList = forexJpaController.findBySymbolAndBetweenDates("AUDUSD", startDate, endDate);
        assertEquals(564, forexList.size());
    }

    @Test
    public void getStartAndDateOfWeekYear() {
        Calendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setFirstDayOfWeek(Calendar.MONDAY);
        gregorianCalendar.setMinimalDaysInFirstWeek(7);

        int numWeekofYear = 40;  //INPUT  
        int year = 2005;         //INPUT  

        gregorianCalendar.set(Calendar.YEAR, year);
        gregorianCalendar.set(Calendar.WEEK_OF_YEAR, numWeekofYear);

        System.out.println(gregorianCalendar.get(Calendar.DAY_OF_MONTH)
                + "/" + (gregorianCalendar.get(Calendar.MONTH)
                + 1) + "/" + gregorianCalendar.get(Calendar.YEAR));
    }

    @Test
    public void generateHourIntervalsFromDates() throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date day = df.parse("2013-07-15 00:00:00");


        SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int hours = 4;
        int numberOfHours = 24 / hours;
        int interval = hours - 1;
        for (int i = 0; i < 24; i = i + hours) {
            Calendar startTime = Calendar.getInstance();
            startTime.setTime(day);
            startTime.add(Calendar.HOUR_OF_DAY, i);


            Calendar endTime = Calendar.getInstance();
            endTime.setTime(startTime.getTime());
            endTime.add(Calendar.HOUR_OF_DAY, interval);
            endTime.add(Calendar.MINUTE, 59);
            endTime.add(Calendar.SECOND, 59);
            System.out.println(df2.format(startTime.getTime()));
            System.out.println(df2.format(endTime.getTime()));
        }


        // Get time intervals for an Hour e.g. 00:00:00 - 00:59:59




    }

    @Test
    public void leftPaddingZeros() {
        for (int i = 0; i < 24; i++) {
            int startHr = i;
            String startHour = String.format("%02d", startHr) + "00";
            System.out.println(startHour);

        }
    }

    @Test
    public void getDatesBetweenStartAndEndDate() throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date endDate = df.parse("2013-07-21");

        int movingAverageInDays = 7;
        TreeSet<Date> days = new TreeSet<Date>();
        days.add(endDate);
        Date today = endDate;
        Calendar yesterday = Calendar.getInstance();
        int ctr = 1;
        while (ctr < movingAverageInDays) {
            yesterday.setTime(today);
            yesterday.add(Calendar.DAY_OF_YEAR, -1);
            today = yesterday.getTime();
            days.add(today);
            ctr++;
        }

        for (Date date : days) {
            System.out.println(df.format(date));
        }
    }

    @Test
    public void treeHashSet() {
        TreeSet<BigDecimal> openSet = new TreeSet<BigDecimal>();
        openSet.add(new BigDecimal("0.9175"));
        openSet.add(new BigDecimal("0.9180"));
        openSet.add(new BigDecimal("0.9180"));
        openSet.add(new BigDecimal("0.9175"));
        openSet.add(new BigDecimal("0.9174"));
        openSet.add(new BigDecimal("0.9169"));
        openSet.add(new BigDecimal("0.9174"));
        openSet.add(new BigDecimal("0.9178"));
        openSet.add(new BigDecimal("0.9180"));
        openSet.add(new BigDecimal("0.9185"));
        openSet.add(new BigDecimal("0.9185"));
        openSet.add(new BigDecimal("0.9188"));

        System.out.println(openSet);
    }

    @Test
    public void getMinimumOpenFromForexList() {
        List<Forex> forexListGroupedByHour = new ArrayList<Forex>();
        Forex forex1 = new Forex();
        forex1.setOpen(new BigDecimal("0.9175"));
        Forex forex2 = new Forex();
        forex2.setOpen(new BigDecimal("0.9180"));
        Forex forex3 = new Forex();
        forex3.setOpen(new BigDecimal("0.9180"));
        Forex forex4 = new Forex();
        forex4.setOpen(new BigDecimal("0.9174"));
        Forex forex6 = new Forex();
        forex6.setOpen(new BigDecimal("0.9169"));
        Forex forex7 = new Forex();
        forex7.setOpen(new BigDecimal("0.9174"));
        Forex forex8 = new Forex();
        forex8.setOpen(new BigDecimal("0.9178"));
        Forex forex9 = new Forex();
        forex9.setOpen(new BigDecimal("0.9180"));
        Forex forex10 = new Forex();
        forex10.setOpen(new BigDecimal("0.9185"));
        Forex forex11 = new Forex();
        forex11.setOpen(new BigDecimal("0.9185"));
        Forex forex12 = new Forex();
        forex12.setOpen(new BigDecimal("0.9188"));


        forexListGroupedByHour.add(forex1);
        forexListGroupedByHour.add(forex2);
        forexListGroupedByHour.add(forex3);
        forexListGroupedByHour.add(forex4);
        forexListGroupedByHour.add(forex6);
        forexListGroupedByHour.add(forex7);
        forexListGroupedByHour.add(forex8);
        forexListGroupedByHour.add(forex9);
        forexListGroupedByHour.add(forex10);
        forexListGroupedByHour.add(forex11);
        forexListGroupedByHour.add(forex12);

        Collections.sort(forexListGroupedByHour, new Comparator<Forex>() {
            @Override
            public int compare(Forex forex1, Forex forex2) {

                int value = forex1.getOpen().compareTo(forex2.getOpen());
                if (value == -1) {
                    return 1;
                }
                return value;
            }
        });

        Forex forex = forexListGroupedByHour.get(0);
        System.out.println(forex.getOpen());
        assertEquals(new BigDecimal("0.9169"), forex.getOpen());
    }

    @Test
    public void getMaximunCloseFromForexList() {
        List<Forex> forexListGroupedByHour = new ArrayList<Forex>();
        Forex forex1 = new Forex();
        forex1.setClose(new BigDecimal("0.1004"));
        Forex forex2 = new Forex();
        forex2.setClose(new BigDecimal("0.1002"));
        Forex forex3 = new Forex();
        forex3.setClose(new BigDecimal("0.1003"));
        Forex forex4 = new Forex();
        forex4.setClose(new BigDecimal("0.1001"));
        forexListGroupedByHour.add(forex1);
        forexListGroupedByHour.add(forex2);
        forexListGroupedByHour.add(forex3);
        forexListGroupedByHour.add(forex4);

        Collections.sort(forexListGroupedByHour, new Comparator<Forex>() {
            @Override
            public int compare(Forex forex1, Forex forex2) {

                int value = forex1.getClose().compareTo(forex2.getClose());
                if (value == -1) {
                    return 1;
                }
                return value;
            }
        });

        Forex forex = forexListGroupedByHour.get(0);
        System.out.println(forex.getClose());
        assertEquals(forex.getClose(), new BigDecimal("0.1004"));
    }
}